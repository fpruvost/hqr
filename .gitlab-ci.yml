image: registry.gitlab.inria.fr/solverstack/docker/distrib

stages:
  - pre
  - build
  - test
  - analyze
  - validate

before_script:
  - git config --global --add safe.directory $CI_PROJECT_DIR
  - git submodule update --init --recursive

.only-master-mr:
  interruptible: true
  rules:
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^ci-.*$/)
    - if: ($CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME !~ /^notest-.*$/)

.only-mr:
  interruptible: true
  rules:
    - if: ($CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME !~ /^notest-.*$/)

preliminary_checks:
  stage: pre
  tags: ["docker", "large"]
  interruptible: true
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  parallel:
    matrix:
      - TEST: [rebase, draft, header]
  script:
    - .gitlab/check_ci.sh $TEST

hqr_build_linux:
  stage: build
  tags: ["docker", "large"]
  extends: .only-master-mr
  variables:
    SYSTEM: linux
  script:
    - bash .gitlab/build.sh | tee hqr-build-linux.log
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    expire_in: 180 minutes
    untracked: true

hqr_build_macosx:
  stage: build
  tags: ['macosx']
  extends: .only-master-mr
  variables:
    SYSTEM: macosx
  script:
    - bash .gitlab/build.sh | tee hqr-build-linux.log
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    expire_in: 180 minutes
    paths:
      - hqr-build-linux.log
  cache:
    key: "${SYSTEM}-${VERSION}-$CI_COMMIT_REF_SLUG"
    untracked: true
    policy: push

hqr_build_windows:
  stage: build
  tags: ['windows']
  extends: .only-master-mr
  variables:
    SYSTEM: windows
    CHERE_INVOKING: "yes"
    MSYSTEM: UCRT64
  script:
    - bash -lc .gitlab/build.sh | tee hqr-build-linux.log
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    expire_in: 180 minutes
    paths:
      - hqr-build-linux.log
  cache:
    key: "${SYSTEM}-${VERSION}-$CI_COMMIT_REF_SLUG"
    untracked: true
    policy: push

hqr_test_linux:
  stage: test
  tags: ["docker", "large"]
  extends: .only-master-mr
  needs: [hqr_build_linux]
  variables:
    SYSTEM: linux
  script:
    - bash .gitlab/test.sh | tee hqr-test-linux.log
  coverage: /^\s*lines......:\s*\d+.\d+\%/
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    expire_in: 180 minutes
    paths:
      - hqr-test-linux.log
      - junit.xml
      - hqr.lcov
      - hqr-coverage.xml
    reports:
      junit: junit.xml

hqr_test_macosx:
  stage: test
  tags: ['macosx']
  extends: .only-master-mr
  needs: [hqr_build_macosx]
  variables:
    SYSTEM: macosx
  script:
    - bash .gitlab/test.sh | tee hqr-test-macosx.log
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    expire_in: 180 minutes
    paths:
      - hqr-test-macosx.log
    reports:
      junit: junit.xml
  cache:
    key: "${SYSTEM}-${VERSION}-$CI_COMMIT_REF_SLUG"
    untracked: true
    policy: pull

hqr_test_windows:
  stage: test
  tags: ['windows']
  extends: .only-master-mr
  needs: [hqr_build_windows]
  variables:
    SYSTEM: windows
    CHERE_INVOKING: "yes"
    MSYSTEM: UCRT64
  script:
    - bash .gitlab/test.sh | tee hqr-test-windows.log
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    expire_in: 180 minutes
    paths:
      - hqr-test-windows.log
    reports:
      junit: junit.xml
  cache:
    key: "${SYSTEM}-${VERSION}-$CI_COMMIT_REF_SLUG"
    untracked: true
    policy: pull

sonarqube:
  stage: analyze
  tags: ["docker", "large"]
  interruptible: true
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^ci-.*$/
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME !~ /^notest-.*$/
      when: manual
      allow_failure: true
  needs: [hqr_build_linux,hqr_test_linux]
  script:
    - ./.gitlab/analysis.sh
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    expire_in: 180 minutes
    paths:
      - filelist.txt
      - hqr-cppcheck.xml
      - hqr-rats.xml
      - sonar-project.properties
      - sonar.log
    when: always

validate:
  stage: validate
  tags: ["docker", "large"]
  extends: .only-mr
  needs: [sonarqube]
  parallel:
    matrix:
      - METRIC: [BUG, COVERAGE]
  script:
    - ./.gitlab/validate.sh $METRIC
  allow_failure: true
